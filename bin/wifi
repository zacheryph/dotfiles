#!/bin/bash
# Script to add/change wifi networks
WPA_CONFIG="/etc/wpa_supplicant/wpa_supplicant.conf"

read -r -d '' USAGE <<EOF
wifi: add wifi networks to wpa_supplicant

usage:
  wifi add <tag> <ssid> - add network configuration
  wifi add-open <ssid>  - add open/unsecured network
  wifi down             - take wifi interface down
  wifi init <iface>     - initialize wpa_supplicant
  wifi list             - list configured networks
  wifi show <tag>       - show network configuration
EOF

usage() {
  echo "$USAGE"
  exit
}

init_wifi() {
  iface="$1"
  conf="/etc/network/interfaces.d/wifi-${iface}"

  if [[ -e "$conf" ]]; then
    echo "!! wifi interface ${iface} already configured"
    echo "=> remove ${conf} to re-initialize"
    exit
  fi

  sudo dd status=none of="$conf" <<EOF
allow-hotplug $iface
iface $iface inet manual
  wpa-driver wext
  wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf
EOF
}

list_network() {
  sudo wpa_cli list_networks
}

show_network() {
  echo "show"
}

add_network() {
  name="$1"
  net="$2"

  read -s -r -p 'Password: ' pswd
  echo

  if ! wpa_output=$(wpa_passphrase "$net" "$pswd"); then
    echo "!! error executing wpa_passphrase"
    echo "=> ${wpa_output}"
    exit
  fi

  sudo dd status=none conv=notrunc oflag=append of="$WPA_CONFIG" <<EOF

# wifi-net: ${name}
${wpa_output}
EOF

  sudo wpa_cli reconfigure
  echo "== added network '$name' for ssid '$net'"
}

add_open_network() {
  name="$1"

  sudo dd status=none conv=notrunc oflag=append of="$WPA_CONFIG" <<EOF

# unsecured-net: ${name}
network={
  ssid="$name"
  key_mgmt=NONE
  priority=-999
}
EOF

  sudo wpa_cli reconfigure
  echo "== added unsecured network '$name'"
}

# main
[[ -z "$1" ]] && usage

case "$1" in
  add)
    [[ -z "$2" || -z "$3" ]] && usage
    add_network "$2" "$3"
    ;;
  add-open)
    [[ -z "$2" ]] && usage
    add_open_network "$2"
    ;;
  down)
    down_interface
    ;;
  init)
    [[ -z "$2" ]] && usage
    init_wifi "$2"
    ;;
  list)
    list_network
    ;;
  show)
    [[ -z "$2" ]] && usage
    show_network "$2"
    ;;
  *)
    usage
    ;;
esac
